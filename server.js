//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');


var bodyparser = require('body-parser');
app.use(bodyparser.json())

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosv2.json');

app.get("/",function(req,res) {
  //res.send("hola mundo node js");
  res.sendFile(path.join(__dirname,"index.html"));
});

app.get("/Clientes",function(req,res) {
  res.send("Aqui estan los clientes devueltos");
});

app.get("/Clientes/:idcliente",function(req,res) {
  res.send("Aqui tiene al cliente numero:" + req.params.idcliente);
});

app.get("/Movimientos/v1/",function(req,res) {
  res.sendfile("movimientosv1.json");
});

app.get("/Movimientos/v2/:idcliente",function(req,res) {
  console.log(req.params.index);
  res.send(movimientosJSON[req.params.index]);
});

app.get("/Movimientosq/v2",function(req,res) {
  console.log(req.query);
  res.send("Recibido");
});

app.get("/Movimientos/v2/",function(req,res) {
  res.send(movimientosJSON);
});

app.post("/", function(req,res) {
  res.send("Hemos recibido su peticion POST");
})

app.post("/Movmientos/v2", function(req,res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta");
  })
